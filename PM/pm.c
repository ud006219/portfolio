#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <stdbool.h>

void recursivePath(char *path);
char pathholder[10000];
char pathcurrent[1000];
char pathnew[1000];

int findTag(char *tagcontent, char *path){
	char tagname[100];
	snprintf(tagname, sizeof tagname,"%s", tagcontent);
	char tagpath[1000];
    snprintf(tagpath, sizeof tagpath,"%s/.pm_tag",path);
    char *filepath = tagpath;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    FILE *fp = fopen(filepath, "r");
    if (!fp) {
        return 1;
    }
    while ((read = getline(&line, &len, fp)) != -1) {
        line[strcspn(line, "\n")] = 0;
        if (!strcmp(line, tagname)) {
            printf("%s\n",tagpath);
			strcpy(pathholder,tagpath);
            break;
        }
    }
    return 0;
}

void fileRecursive(char* directory, char* tag)
{
	char basepath[10000];
	snprintf(basepath, sizeof basepath,"%s",directory);
    char checktag[100];
	snprintf(checktag, sizeof checktag,"%s",tag);
	char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basepath);

   
    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {   
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strlen(dp->d_name) < 15 && strlen(dp->d_name) >2)
        {
//          printf("%s\n", dp->d_name);
            strcpy(path, basepath);
            strcat(path, "/");
            strcat(path, dp->d_name);
            findTag(checktag,path);
            fileRecursive(path,checktag);
        }
    }

    closedir(dir);
}

void removeSubstr (char *string, char *sub) {
    char *match;
    int len = strlen(sub);
    while ((match = strstr(string, sub))) {
        *match = '\0';
        strcat(string, match+len);
    }
}

int moveFeature(char* tagcurrent, char* tagnew){
	fileRecursive(".",tagcurrent);
	strcpy(pathcurrent,pathholder);
	removeSubstr(pathcurrent,".pm_tag");
	fileRecursive(".",tagnew);
	strcpy(pathnew,pathholder);
	removeSubstr(pathnew,".pm_tag");
	char command[10000];
	snprintf(command, sizeof command,"mv %s %s",pathcurrent,pathnew);
	system(command);
	return 0;
}

int renameDir(char* currentname, char* newname){
	char command[1000];
	snprintf(command, sizeof command, "find . -depth -type d -name %s -execdir mv {} %s \\;", currentname, newname);
	system(command);
	return 0;
}

int AddTag(char *argv){
	FILE *file;
    if ((file = fopen(".pm_tag", "r"))){
        fclose(file);
        printf("Already a tag for this folder.");
		system("cat .pm_tag");
		return 1;
    }
	else{
    file = fopen(".pm_tag", "w");
    fputs(argv, file);
	fclose(file);
	return (0);
	}
}

int MakeFeature(char *argv){
	mkdir(argv, 0777);
	char command[128];
	snprintf(command, sizeof command, "cd %s", argv);
	system(command);
	snprintf(command, sizeof command, "cd %s && git checkout -b %s",argv,argv);
	system(command);
	snprintf(command, sizeof command, "mkdir %s/bin", argv);
	system(command);
	snprintf(command, sizeof command, "mkdir %s/doc", argv);
	system(command);
	snprintf(command, sizeof command, "mkdir %s/lib", argv);
	system(command);
	snprintf(command, sizeof command, "mkdir %s/src", argv);
	system(command);
	snprintf(command, sizeof command, "mkdir %s/tests", argv);
	system(command);
	return 0;
}

int main(int argc,char **argv){
	if (strcmp(argv[1], "create_project") == 0){
		char folderName[64];
		strcpy(folderName, argv[2]);
		if(mkdir(folderName, 0777)==-1){
			//This checks if name of file/directory already exists
			if (errno == EEXIST){
				printf("A folder of that name already exists. Aborting.");
			}
			return 1;
		}
		//Creates necessary subdirectiories in local git repo initiated
		char command[256];
		snprintf(command, sizeof command, "git init %s", folderName);
		system(command);
		snprintf(command, sizeof command, "mkdir %s/bin", folderName);
		system(command);
		snprintf(command, sizeof command, "mkdir %s/doc", folderName);
		system(command);
		snprintf(command, sizeof command, "mkdir %s/lib", folderName);
		system(command);
		snprintf(command, sizeof command, "mkdir %s/src", folderName);
		system(command);
		snprintf(command, sizeof command, "mkdir %s/tests", folderName);
		system(command);
		char userEmail[64];
		char userID[20];
		printf("Input user email: ");
		scanf("%s", userEmail);
		printf("Input user name: ");
		scanf("%s", userID);
		snprintf(command, sizeof command, "cd %s && git config --global user.email\"%s\"", folderName,userEmail);
		system(command);
		snprintf(command, sizeof command, "cd %s && git config --global user.name \"%s\"",folderName,userID);
		system(command);
		snprintf(command, sizeof command, "cd %s && git remote add origin https://csgitlab.reading.ac.uk/%s/%s.git",folderName,userID,folderName);
		system(command);
		return 0;
	}
	if (strcmp(argv[1], "add_feature") == 0){
		char featureName[64];
		strcpy(featureName, argv[2]);
		MakeFeature(featureName);
	}
	if (strcmp(argv[1], "add_tag") == 0){
		char tag[10];
		strcpy(tag, argv[2]);
		AddTag(tag);
	}
	if (strcmp(argv[1], "find_tag") == 0){	
		char tagname[10];
		strcpy(tagname,argv[2]);
		fileRecursive(".",tagname);
	}
	if (strcmp(argv[1], "rename_feature") == 0){
		char currentname[10000];
		strcpy(currentname,argv[2]);
		char newname[10000];
		strcpy(newname, argv[3]);
		renameDir(currentname, newname);
	}
	if (strcmp(argv[1], "move_by_tag") == 0){
		char tagcurrent[100];
		strcpy(tagcurrent, argv[2]);
		char tagnew[100];
		strcpy(tagnew, argv[3]);
		moveFeature(tagcurrent, tagnew);
	}
}
